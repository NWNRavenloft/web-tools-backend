import test from 'ava';
import * as funcs from './authentication';

test('my passing test', async t => {
	let ctx = {request: {body: {}}};

	await funcs.authenticate(ctx);
	t.pass();
});