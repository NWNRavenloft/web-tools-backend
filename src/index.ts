var Koa = require('koa');
var Router = require('@koa/router');
var mysql = require('koa2-mysql-wrapper');
const koaBody = require('koa-body');
var session = require('koa-session')
const mongo = require('koa-mongo')

var config = require('../config.json');
var player_list = require('./player-list.ts')
var authentication = require('./authentication.ts')
var notes_for_dms = require('./notes-for-dms.ts')

var app = new Koa();
var router = new Router();

async function cors(ctx: any, next: any) {
    await next();
    ctx.set('Access-Control-Allow-Origin', '*');
}
app.use(mongo({
  host: 'localhost',
  port: 27017,
  db: 'potm-tools',
}));

app.use(cors);
app.use(koaBody());
app.use(mysql({host:config.db_host, 
    user: config.db_user,
    password: config.db_password,
    database: config.db_name}))

const SESSION_CONFIG = {
    key: config.cookie_pass,
    renew: true
};

app.keys = [config.cookie_pass];

app.use(session(SESSION_CONFIG, app))

router.get('/area', async function(ctx: any, next: any) {
    try {
        let result = await ctx.myPool().query('SELECT * FROM stored_areas');

        // Output test result (3)
        ctx.body = result;
    } catch (err) {
        // 500 Internal Server Error
        console.log("Error: " + err);
        ctx.status = 500;
        ctx.body = {
            error: err
        };
    }
    await next();
});

router.post('/area', async function(ctx: any, next: any) {
    try {
        // This is our SQL query script
        let query = "INSERT INTO stored_areas (id, data) VALUES(1, ?) ON DUPLICATE KEY UPDATE data=?;";

        let result = await ctx.myPool().query(query, [ctx.request.body, ctx.request.body]);

        console.log(result);
        ctx.body = result;
    } catch (err) {
        // 500 Internal Server Error
        console.log("Error: " + err);
        ctx.status = 500;
        ctx.body = {
            error: err
        };
    }
    await next();
});

const redis = require("redis"),
        subscriber = redis.createClient("6379", "46.4.59.55"),
        publisher = redis.createClient("6379", "46.4.59.55");

const dm_messages: any = []

// This needs rework when adding more redis stuff - right now we're not checking for channel.
// move subscriber.on to redis.js.
subscriber.on("message", function(channel: string, message: string) {
        dm_messages.push(message);
        console.log("GOT A DM MESSAGE: " + message);
        if (dm_messages.length > 20)
        	dm_messages.pop();
});

subscriber.subscribe("nwserver.chat.dm_channel");

router.get('/dm-messages-lZsC', async function(ctx: any, next: any) {
    ctx.response.type = "application/json";
    ctx.body = JSON.stringify(dm_messages);

    await next();
});

router.get('/dm-messages', async function(ctx: any, next: any) {

    if (ctx.session.dm)
    {
        console.log("Sending list of msgs..");
        ctx.response.type = "application/json";
        ctx.body = JSON.stringify(dm_messages);
    }
    else
    {
        console.log("Not logged in!");
        ctx.response.type = "application/json";
        ctx.body = JSON.stringify({});
    }

    await next();
});

app.use(router.routes())
app.use(player_list.routes);
app.use(authentication.routes);
app.use(notes_for_dms.routes);

app.listen(config.port);