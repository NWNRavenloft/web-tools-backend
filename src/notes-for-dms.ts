var Router = require('@koa/router');

var router = new Router();

const shortid = require('shortid');

router.get('/note/:id', async function(ctx: any, next: any) {
	if (ctx.session.dm)
	{
		ctx.body = await ctx.db.collection('notes').findOne({_id: ctx.params.id});
	}
	else
	{
		ctx.body = {error: "Only DMs may view these notes."};
	}

	await next();
});

router.post('/save-note', async function(ctx: any, next: any) {

	console.log("hete");
	if (!ctx.request.body.text)
	{
		console.log("Returning error");
		ctx.status = 500;
        ctx.body = {
            error: "Error"
        };
		return;
	}
	const id = shortid.generate();
	const result = await ctx.db.collection('notes').insertOne({text: ctx.request.body.text, _id: id});
	ctx.body = id;
	//ctx.db.collection('users').remove({
	//	_id: mongo.ObjectId(userId)
	//})

    await next();
});

exports.routes = router.routes();