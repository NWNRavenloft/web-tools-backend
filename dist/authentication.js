"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Router = require('@koa/router');
var router = new Router();
const argon2 = require('argon2');
router.post('authenticate', '/authenticate', async function (ctx, next) {
    console.log(ctx.request.body);
    await authenticate(ctx);
    await next();
});
async function authenticate(ctx) {
    console.log("In authenticate");
    let dm = await argon2.verify("$argon2i$v=19$m=4096,t=3,p=1$NshgFx5U8AxQ+qZSFyGrIw$1/cXongP4cV+3tmis3Y7Q8xmCpOk18JSXj8Rb3W4juk", ctx.request.body.password);
    if (dm) {
        console.log("Logging a DM in..");
        ctx.session.dm = true;
    }
    else {
        console.log("Wrong password!");
    }
}
exports.authenticate = authenticate;
let routes = router.routes();
//# sourceMappingURL=authentication.js.map