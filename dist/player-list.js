var Router = require('@koa/router');
var config = require('../config.json');
var router = new Router();
router.get('/player-list', async function (ctx, next) {
    try {
        let result = await ctx.myPool().query('SELECT * FROM players_online');
        // Output test result (3)
        ctx.body = result;
    }
    catch (err) {
        // 500 Internal Server Error
        console.log("Error: " + err);
        ctx.status = 500;
        ctx.body = {
            error: err
        };
    }
    await next();
});
exports.routes = router.routes();
//# sourceMappingURL=player-list.js.map