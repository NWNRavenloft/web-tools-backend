"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const ava_1 = require("ava");
const funcs = require("./authentication");
ava_1.default('my passing test', async (t) => {
    let ctx = { request: { body: {} } };
    await funcs.authenticate(ctx);
    t.pass();
});
//# sourceMappingURL=authentication.test.js.map